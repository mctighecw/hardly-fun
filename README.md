# README

A little project, just for fun, using simple old-school web tools, including a bit of jQuery. It shows something random that is _Hardly Fun_.

## App Information

App Name: hardly-fun

Created: April 2020

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/hardly-fun)

Production: [Link](https://hardly-fun.netlify.app)

## Tech Stack

- HTML5
- CSS3
- jQuery
- JavaScript (ES5)

Last updated: 2025-02-05
