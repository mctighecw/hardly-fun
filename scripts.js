// ES5 compatibility
var notFunThings = [
  "fall down the stairs",
  "slip on a banana peel",
  "forget your keys",
  "spill soup on your shirt",
  "step on a beehive",
  "run out of gas",
  "eat bad tacos",
  "find a hair in your salad",
  "miss the train",
  "shatter your phone's screen",
  "forget your email password",
  "step in dog doo",
  "get unfriended on Facebook",
  "miss the last bus home",
  "get a speeding ticket",
  "oversleep and miss a job interview",
  "eat spoiled oysters",
  "step on chewing gum",
  "drop your ice cream on the ground",
  "lose your wallet",
  "get a splinter in your finger",
  "spill ketchup on your shorts",
  "stub your toe on a rock",
  "fall off your bike",
  "get locked out of your apartment",
  "get caught in the rain without an umbrella",
  "get sick on vacation",
  "run out of toilet paper",
  "get bitten by a raccoon",
  "get a parking ticket",
  "step on a thumbtack",
  "burn your hand on the stove",
  "trip on the carpet",
  "lose your phone in a taxi",
  "get a blister on your foot"
];

var unhappyEmojis = [
  "😐",
  "😑",
  "😒",
  "😕",
  "🙁",
  "😮",
  "😦",
  "😟",
  "😬",
  "😞",
  "😔",
  "😖",
  "😥",
  "😢",
  "😭"
];

function getRandomNumber(array) {
  var max = array.length - 1;
  var res = Math.floor(Math.random() * (max + 1));
  return res;
}

function loadSentence() {
  var in1 = getRandomNumber(notFunThings);
  var in2 = getRandomNumber(unhappyEmojis);
  var previousIn1 = localStorage.getItem("previousIn1");
  previousIn1 = parseInt(previousIn1, 10);

  // Avoid the same two in a row
  if (in1 === previousIn1) {
    if (in1 === notFunThings.length - 1) {
      in1 = 0;
    } else {
      in1 += 1;
    }
  }
  localStorage.setItem("previousIn1", in1);

  var item = notFunThings[in1];
  var emoji = unhappyEmojis[in2];

  var text =
    '<div class="text">' +
    item +
    '<span class="emoji">' +
    emoji +
    "</span>" +
    "</div>";

  var sentence =
    '<div class="sentence">' + "It's hardly fun to..." + text + "</div>";

  setTimeout(function() {
    $(".text").addClass("visible");
  }, 750);
  document.getElementById("container").innerHTML = sentence;
}

$(document).ready(function() {
  loadSentence();

  setInterval(function() {
    loadSentence();
  }, 10000);
});
